# Fxbackoffice

Currency exchange demo.

There are two main commands:

* _currency:update_ - Update currencies exchange rates
* _currency:exchange_ - exchange currency

And two installation & usage ways: local PHP and Docker.

## 1. Local-way

### Requirements

* PHP 8.1
    * ext-bcmath
    * ext-ctype
    * ext-iconv
    * ext-intl
    * ext-simplexml
* Composer

### Installation

```bash
git clone https://vladimirmartsul@bitbucket.org/vladimirmartsul/fxbackoffice.git
cd fxbackoffice
composer install --no-dev --no-interaction
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate --no-interaction
```

### Usage

Update currency rates first

```bash
php bin/console currency:update
```

Use like (in project directory)

```bash
php bin/console currency:exchange <amount> <from> <to>
```

For example

```bash
php bin/console currency:exchange 2 EUR BTC
```

should output

```bash
[OK] 2 EUR is 0.00005254 BTC
```

### Testing

```bash
cd fxbackoffice
echo APP_ENV=test > .env.local
composer install --no-interaction
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate --no-interaction
vendor/bin/phpunit
```

## 2. Docker-way

### Requirements

Docker Desktop (Windows, MacOS) or docker-cli and docker-compose (Linux)

### Installation

```bash
git clone https://vladimirmartsul@bitbucket.org/vladimirmartsul/fxbackoffice.git
cd fxbackoffice
docker compose build
```

Currency rates will be updated on build.

### Usage

Use like (in project directory)

```bash
docker compose run fxbackoffice currency:exchange <amount> <from> <to>
```

For example

```bash
docker compose run fxbackoffice currency:exchange 2 EUR BTC
```

should output

```bash
[OK] 2 EUR is 0.00005254 BTC
```

### Testing

```bash
cd fxbackoffice
echo "APP_ENV=test" > .env.local
docker compose run fxbackoffice composer install --no-interaction
docker compose run fxbackoffice doctrine:database:create
docker compose run fxbackoffice doctrine:migrations:migrate --no-interaction
docker compose run fxbackoffice vendor/bin/phpunit
```

## Extending

There is ability to add new exchange rates providers.

The new provider must implement `\App\Contracts\RatesProviderInterface` interface with constructor `__construct(\Symfony\Contracts\HttpClient\HttpClientInterface $client, string $url, string $base)` and `__invoke(): App\Dto\Rate[]` method.

However, in many cases it is enough to extend abstract `\App\Providers\RatesProvider` class and
implement `\App\Providers\RatesProvider::transform(array $data): RateDto[]` method.

Additionaly the new provider must be registered in _config/services.yaml_ with its own arguments and `app.rates_provider` tag. Real params such as URL and base currency must be writen in _.env_ file.

## Keynotes

* Using a small Symfony installation as possible
* Using SQLite database for simplicity but with price of some caveats
  * There are no real decimal or numeric data type so emulation with harcoded 16,8 precision is used
  * There are no INSERT IGNORE method so unique constraint violation are just skipped
* There is `\App\Services\RatesUpdater::makeReverseRate` method to make reverse exchange rate. However, separate buy and sell rates should be used in real life.
* There are two tables for rates: _**rates**_ with provided data and _**pairs**_ with all currencies combinations (triangles). Triangulations are implemented in the `\App\Services\RatesTriangulator` service. It's inspired by http://www.dpxo.net/articles/fx_rate_triangulation_sql.html
* There was some **date** errors in ECB rates so rates' date are writing to the tables but not using in the finally exchange calculation
* There are simple functional tests with fake data from `\App\Providers\FakeRatesProvider` provider
